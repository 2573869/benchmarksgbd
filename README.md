## Name
BenchmarkSGBD

## Description
This project has 3 repositories : data, script and docs. In the repository data, you can find 2 datasets with their source and the data transformed in Temporal Graph, also the program made for the data processing. In the repository script, there are several scripts that may be used to  achieve additional objectifs like export RDF format data. In the end, you can find all the documentations, installation instructions, etc. 

## Support
Zijian LI : zijian_li1273@163.com


