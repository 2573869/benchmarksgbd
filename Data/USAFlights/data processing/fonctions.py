#this is a function script which can determine if the data has the right format
#if the format is correct, this function will transform the timestamp into a standard format
#if the row has no data, this function will replace the row by a string
import datetime
import pandas as pd
import math

def transformat(time):
 try:
   #Firstly determine if the time data is null, if not, make it a integer format
  if not (math.isnan(time)) and not (time is None):
     time = int(float(time))
    #clean and optimize the format
     if not math.isnan(time):
      if time<1000:
       time = "0" + str(time)
      else:
       time = str(time)

     if time=="2400":
         time ="0000"
    #transform into a standard time format
     if datetime.datetime.strptime(time, '%H%M'):
       time1 = str(datetime.datetime.strptime(time, '%H%M').strftime('%H:%M'))
       #print("format bon " + time1)
       return time1
    #if null, give a string value to the null row
  else:
    time1 = "Null"
    print("valeur vide " + time1)
    return time1
 except Exception as e:
     pass
