#this script allows the user to extract and transform the data from original airport dataset to temporal graph 
#the transformed data will be stored in the same directory as "aeroport.csv" at the output
import pandas as pd
import time
import os

path = "/home/activus01/data/aero/airports.csv"
data = pd.read_csv(path, encoding='utf-8',sep=",")
st = time.time()
Obj = pd.DataFrame(columns=['aeroportid:ID(Aeroport)', 'libelleap', 'villeap', 'latap', 'longtap', 'startvalidtime',
                                'endvalidtime', ':LABEL'])
idap=data["iata"].tolist()
libelleap=data["airport"].tolist()
villeap=data["city"].tolist()
lat=data["lat"].tolist()
long=data["long"].tolist()
startvalidtime = ["2000-01-01T00:00:00.000" for i in range(0,len(idap))]
endvalidtime = ["" for i in range(0,len(idap))]
# label
label=["Aeroport" for i in range (0,len(idap))]
# combine dataframe

Obj = {'aeroportid:ID(Aeroport)': idap, 'libelleap': libelleap, 'villeap': villeap, 'latap': lat, 'longtap':long, 'startvalidtime': startvalidtime,
                  'endvalidtime': endvalidtime, ':LABEL':label}

data=pd.DataFrame(Obj)
print(data)
# delete duplication
#data.drop_duplicates(keep="first", inplace=True)
# save file
data.to_csv('/home/activus01/data/aero/aeroport.csv', index=False, sep=',', encoding='utf-8')

et = time.time()
cost_time = et - st
print('file cost time：{}seconds'.format(float('%.2f' % cost_time)))
