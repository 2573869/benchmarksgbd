#Must execute this script before import data into postgresql
#this script is to process the data in order to fit the RDBMS principles
import pandas as pd
data=pd.read_csv('/home/activus01/Téléchargements/volnet2000.csv',chunksize=3000)
for chunk in data:
    for index, row in chunk.iterrows():
        #firstly rename the columns
        chunk.columns=['idVol','Depart','Arrive','Distance','Etat','Retard','DepTimeEstime','ArrTimeEstime','DepTimeReel','ArrTimeReel','Type']
        print(row)
        #then drop some duplicates caused by airline states
        data1=chunk.drop_duplicates(subset=['idVol','DepTimeReel','ArrTimeReel'],keep='first',inplace=False)
        data1=data1.drop(['Type'],axis=1)
        data1.to_csv('/home/activus01/Téléchargements/volnet2000new.csv', index=False, sep=',', header=False, mode='a', encoding='utf-8')