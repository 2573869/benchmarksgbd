#this script allows the user to extract and transform the data from original airlines record dataset
#the transformed file will be stored in the same repository 
import pandas as pd
import datetime
from fonctions import transformat
import time
import math

#divide the file into several chunks, then proceed the data processing
data = pd.read_csv("/home/activus01/data/aero/vols/2003.csv", encoding='latin-1', sep=",", chunksize=15000)
st = time.time()
for chunk in data:
    for index, row in chunk.iterrows():
        #get the value by columns
        row["start_time_rel"] = str(row["DayofMonth"]) + "/" + str(row["Month"]) + "/" + str(row["Year"])
        sttrel = row["start_time_rel"]
        deptest = row["CRSDepTime"]
        dept = row["DepTime"]
        arrtest = row["CRSArrTime"]
        arrt = row["ArrTime"]
        id = str(row["UniqueCarrier"]) + str(row["FlightNum"])
        Org = row["Origin"]
        Dest = row["Dest"]
        Distance = row["Distance"]
        if row["Cancelled"] == 1:
            Etat = "Cancelled"
        if row["Diverted"] == 1:
            Etat = "Diverted"
        else:
            Etat = "In time"
        if not math.isnan(row["ArrDelay"]) and not math.isnan(row["DepDelay"]):
            Retard = str(int(row["ArrDelay"]) + int(row["DepDelay"]))
        else:
            Retard = "0"

        #Deal with time
        if Etat == "In time" or Etat == "Diverted":
            de = transformat(deptest)
            dr = transformat(dept)
            print(dr)
            are = transformat(arrtest)
            arr = transformat(arrt)
            if dr == "Null" or arr == "Null":
                deptR = "Null"
                arrR = "Null"
            else:
                deptE = datetime.datetime.strptime(sttrel + de, '%d/%m/%Y%H:%M').strftime('%d/%m/%Y %H:%M')
                deptR = datetime.datetime.strptime(sttrel + dr, '%d/%m/%Y%H:%M').strftime('%d/%m/%Y %H:%M')
                arrE = datetime.datetime.strptime(sttrel + are, '%d/%m/%Y%H:%M').strftime('%d/%m/%Y %H:%M')
                arrR = datetime.datetime.strptime(sttrel + arr, '%d/%m/%Y%H:%M').strftime('%d/%m/%Y %H:%M')
        else:
            de = transformat(deptest)
            dr = "NULL"
            are = transformat(arrtest)
            arr = "NULL"
            deptE = datetime.datetime.strptime(sttrel + de, '%d/%m/%Y%H:%M').strftime('%d/%m/%Y %H:%M')
            deptR = "NULL"
            arrE = datetime.datetime.strptime(sttrel + are, '%d/%m/%Y%H:%M').strftime('%d/%m/%Y %H:%M')
            arrR = "NULL"
        label= ":Vol"
        
        # save df
        fichier = pd.DataFrame(
            {"idVol": [id], "Depart": [Org], "Destination": [Dest], "Distance": [Distance], "Etat": [Etat],
             "Retard": [Retard + "min"], "DepTimeEstime": [deptE],
             "DepTimeReel": [deptR], "ArrTimeEstime": [arrE],
             "ArrTimeReel": [arrR],":LABEL":[label]})
        print(fichier)
        # save csv
        fichier.to_csv('/home/activus01/data/aero/vols/volnet2003.csv', index=False, sep=',', header=True, mode='a', encoding='utf-8')
    et = time.time()
    cost_time = et - st
    print('file cost time：{}seconds'.format(float('%.2f' % cost_time)))
