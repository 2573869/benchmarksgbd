# generate the file csv for the node 'item'
import pandas as pd
import numpy as np
import time
import os
from timestamp import convertTimeDayNoFile
import csv
#2009-03_00
#2020-10_01
path = "/home/activus01/Téléchargements/2020-10_00.csv"
data = pd.read_table(path, encoding='utf-8',sep=",",chunksize=50000)
st = time.time()
for chunk in data:
 for index, row in chunk.iterrows():
    # colonne valid time: temps minimal de la dataset
    startvalidtime = '2020-10-01T00:00:00'
    endvalidtime = ""
    # label
    label = "Destination"
    # combine dataframe
    addline=pd.DataFrame(columns=['destination id:ID(Destination)', 'startvalidtime',
                        'endvalidtime', ':LABEL'])
    addline=addline.append({'source id:ID(Source)': row["destination_address"], 'startvalidtime': startvalidtime,
                        'endvalidtime': endvalidtime, ':LABEL': label},ignore_index=True)
    print(addline)
    data=pd.DataFrame(addline)
    data.drop_duplicates(subset=['destination id:ID(Destination)'], keep="first", inplace=True)
    data.to_csv('/home/activus01/data/bitcoin/destination.csv',index=False, sep=",",mode='a',header=False)

et = time.time()
cost_time = et - st
print(cost_time)