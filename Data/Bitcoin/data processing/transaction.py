# generate the file csv for the node 'item'
import pandas as pd
import numpy as np
import time
import os
from timestamp import timeStampDayNoZero
import csv
#2009-03_00
#2020-10_01
path = "/home/activus01/Téléchargements/2020-10_00.csv"
data = pd.read_table(path, encoding='utf-8',sep=",",chunksize=1000)
st = time.time()
for chunk in data:
    for index, row in chunk.iterrows():
        # colonne valid time: temps minimal de la dataset
        startvalidtime = timeStampDayNoZero(row["timestamp"])
        endvalidtime = startvalidtime
        type_relation = 'Transaction'
        # combine dataframe
        addline=pd.DataFrame(columns=['source id:ID(Source)', 'startvalidtime',
                            'endvalidtime', ':LABEL'])
        addline=addline.append({'source id:ID(Source)': row["source_address"], 'destination id:ID(Destination)': row["destination_address"],'startvalidtime': startvalidtime,
                            'endvalidtime': endvalidtime, ':TYPE': type_relation},ignore_index=True)
        print(addline)
        data = pd.DataFrame(addline)
        data.drop_duplicates( keep="first", inplace=True)
        data.to_csv('/home/activus01/data/bitcoin/transaction.csv',index=False, sep=",",mode='a',header=False)
et = time.time()
cost_time = et - st
print(cost_time)