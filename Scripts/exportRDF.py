import requests
import os
import json
import random

####social experiment collection####
rdf_host = "http://localhost:7474"#connection to database
post_pstfx = "/rdf/socialexp.db/cypher" #Modifie if necessary
credentials=("neo4j", "lzj980426")#Modifie if necessary
result = open("D:/Datasets/call.ttls",mode='w')#Result storage path
post_url = rdf_host + post_pstfx
post_data = {
     "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:Call]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
     "cypherParams": {},
     "format": "Turtle-star"
}

# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:Access]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:BlogLivejournalTwitter]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:CloseFriend]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:FacebookAllTaggedPhotos]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:Access]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:PoliticalDiscussant]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:Proximity]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:ReceiveSMS]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:SendSMS]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:SocializeTwicePerWeek]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#

post_response = requests.post(post_url, auth=credentials, json=post_data)
result.write(post_response.text)


#####collection citibike####
# rdf_host = "http://localhost:7474"
# post_pstfx = "/rdf/citibike.db/cypher" #Modifie if necessary
# credentials=("neo4j", "lzj980426")#Modifie if necessary
# result = open("D:/Datasets/trip.ttls",mode='w')#Result storage path
# post_url = rdf_host + post_pstfx
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:Trip]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
# post_response = requests.post(post_url, auth=credentials, json=post_data)
# result.write(post_response.text)


#####collection ecommerce####
# rdf_host = "http://localhost:7474"
# post_pstfx = "/rdf/ecommerce.db/cypher" #Modifie if necessary
# credentials=("neo4j", "lzj980426")#Modifie if necessary
# result = open("D:/Datasets/view.ttls",mode='w')#Result storage path
# post_url = rdf_host + post_pstfx
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:view]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:addtocart]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:belongto]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:subcategory]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
#
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:transaction]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }
# post_response = requests.post(post_url, auth=credentials, json=post_data)
# result.write(post_response.text)


###us airlines collection
# rdf_host = "http://localhost:7474"
# post_pstfx = "/rdf/vol.db/cypher" #Modifie if necessary
# credentials=("neo4j", "lzj980426")#Modifie if necessary
# result = open("D:/Datasets/vols.ttls",mode='w')#Result storage path
# post_url = rdf_host + post_pstfx
# post_data = {
#      "cypher": "CALL apoc.cypher.run('MATCH p=()-[r:vol]->() RETURN r ',{}) YIELD value RETURN value.r AS val;",
#      "cypherParams": {},
#      "format": "Turtle-star"
# }

# post_response = requests.post(post_url, auth=credentials, json=post_data)
# result.write(post_response.text)
