import py2neo as pn
import pandas as pd

graph = pn.Graph("bolt://localhost:7687", auth=("neo4j", "lzj980426"))
query = """CALL apoc.periodic.commit("MATCH p = ()-[r:View]-() WITH r limit $limit SET r.instanceitemid =r.instanceitemid+'/'+ r.startvalidtime+'/'+ r.endvalidtime SET r.userid =r.userid+'/'+ r.startvalidtime+'/'+ r.endvalidtime REMOVE r.startvalidtime REMOVE r.endvalidtime RETURN count(*)",{limit: 10000}) YIELD updates, executions,runtime,batches RETURN updates, executions, runtime,batches;"""
graph.run(query)
